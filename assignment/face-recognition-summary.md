**How facial recognition works**

You might be good at recognizing faces. You probably find it a cinch to identify the face of a family member, friend, or acquaintance. You’re familiar with their facial features — their eyes, nose, mouth — and how they come together.

That’s how a facial recognition system works, but on a grand, algorithmic scale. Where you see a face, recognition technology sees data. That data can be stored and accessed. For instance, half of all American adults have their images stored in one or more facial-recognition databases that law enforcement agencies can search, according to a Georgetown University study.

So how does facial recognition work? Technologies vary, but here are the basic steps:

**Step 1.** A picture of your face is captured from a photo or video. Your face might appear alone or in a crowd. Your image may show you looking straight ahead or nearly in profile.

**Step 2.** Facial recognition software reads the geometry of your face. Key factors include the distance between your eyes and the distance from forehead to chin. The software identifies facial landmarks — one system identifies 68 of them — that are key to distinguishing your face. The result: your facial signature.

[Click Here](https://www.pandasecurity.com/mediacenter/src/uploads/2019/10/how-does-facial-recognition-work.mp4)

**Step 3.** Your facial signature — a mathematical formula — is compared to a database of known faces. And consider this: at least 117 million Americans have images of their faces in one or more police databases. According to a May 2018 report, the FBI has had access to 412 million facial images for searches.

**Step 4.** A determination is made. Your faceprint may match that of an image in a facial recognition system database.

