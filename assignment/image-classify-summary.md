**Image Classification**

Image Classification is a fundamental task that attempts to comprehend an entire image as a whole. The goal is to classify the image by assigning it to a specific label. 

[Click Here](https://cdn-gcp.marutitech.com/wp-media/2017/06/Image-classification-process.jpg)


**Working**


**Step 1: Pre-processing**

First, we need to add a little bit of variance to the data since the images from the dataset are very organized and contain little to no noise. We’re going to artificially add noise using a Python library named imgaug. We’re going to do a random combination of the following to the images:

- Crop parts of the image

- Flip image horizontally

- Adjust hue, contrast and saturation

[Clcik Here](https://miro.medium.com/max/700/0*SrTDpcBYxHhp7MSI.)

**Step 2: Splitting our dataset**

It takes a long time to calculate the gradient of the model using the entirety of a large dataset . We therefore will use a small batch of images during each iteration of the optimizer.

**Step 3: Building a Convolutional Neural Network**

Now that we’re done pre-processing and splitting our dataset we can start implementing our neural network. We’re going to have 3 convolution layers with 2 x 2 max-pooling.

[Click Here](https://miro.medium.com/max/700/0*DE9DJZPMpfy0Hmo5.)