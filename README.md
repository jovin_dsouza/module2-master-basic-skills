# Module2 - Master Basic skills

### Goal of the module 
Get Familiar with basics of different AI techniques

1. Basic working of the AI techniques for :
   - Face Detection 
   - Face Recognition
   - Object Detection
   - Segmentation
   - Pose Estimation
   - Image classification

### How to complete this Module
You will learn  about these technologies from the internet and submit a summary the 20 hour way to anchor the concept for yourself . 
More details are in the summary issue : 
You need to submit atleast 3 Techniques though you may submit all. We will check all and best ones will be featured in the community with credits to you .

You need to submit a summary assignment which you will get in [issue1](https://gitlab.com/iotiotdotin/project-internship-ai/module2-master-basic-skills/-/issues/1).

